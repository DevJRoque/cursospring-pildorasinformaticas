package es.pildoras.spring.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HolaAlumnosControlador {

	@RequestMapping("/Formulario")
	public String muestraFormulario() { //Proporciona el formulario
		
		return "HolaAlumnosFormulario";
		
	}
	
	@RequestMapping("/Procesado")
	public String procesarFormulario() {
		
		return "FormularioProcesado";
		
	}
	
	@RequestMapping("/Procesado2")
	public String otroProcesoFormulario(HttpServletRequest request, Model modelo) {
	
		 String nombre=request.getParameter("nombreAlumno");
		
		nombre+=" es el mejor ninja de konoha .";
		
		String mensajeFinal="�Quien es el mejor ninja? " + nombre;
		
		//Agregando info al modelo
		
		modelo.addAttribute("mensaje", mensajeFinal);
		
		return "FormularioProcesado";
		
	}
}
