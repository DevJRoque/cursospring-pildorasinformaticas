package es.pildoras.pruebaAnnotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Cargar el xml de configuracion
		
		ClassPathXmlApplicationContext contexto4=new ClassPathXmlApplicationContext("applicationContext2.xml");
		//Pedir el bean al contenedor
		
		ComercialExperimentado Joshua=contexto4.getBean("ComercialExperimentado",ComercialExperimentado.class);
		//Usar bean
		
		System.out.println(Joshua.getTareas());
		System.out.println(Joshua.getInforme());
		
		//Cerrar el contexto
		
		contexto4.close();
	}

}
