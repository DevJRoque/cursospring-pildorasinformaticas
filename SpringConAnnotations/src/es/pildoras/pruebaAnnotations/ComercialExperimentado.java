package es.pildoras.pruebaAnnotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//Para crear bean con el alias dentro 
@Component("ComercialExperimentado")//Puedo omitir el id,pero la primera letra en minuscula.

//Para cambiar la manera de como se brindaran los objetos, patron de dise�o singleton x defecto o prototype
//@Scope("singleton")

public class ComercialExperimentado implements Empleados {

	@Autowired
	@Qualifier("informeFinancieroTrim4")//Para inyectarlo por constructor o de otra manera se puede prescindir del @Qualifier si pones bien el id 
	private CreacionInformeFinanciero nuevoInforme;//informeFinancieroTrim3
		
	public ComercialExperimentado() {
	
		
	}
	
	//Se puede omitir el autowired si solo es un constructor y funciona con 1 o mas parametros
	/*@Autowired
	public ComercialExperimentado(CreacionInformeFinanciero nuevoInforme) {
		
		this.nuevoInforme=nuevoInforme;
		
	}*/
	
	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Vender, vender y vender m�s";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return nuevoInforme.getInformeFinanciero();
	}
	//En metodos no se puede omitir la anotacion, da error.
	/*@Autowired
	public void setInforme(CreacionInformeFinanciero nuevoInforme) {
		
		this.nuevoInforme=nuevoInforme;
	}*/
	
	//Ejecucion de codigo despues de la creacion del bean
	
	@PostConstruct
	//Este metodo puede tener cualquier modificador de acceso y puede devolver cualquier valor, lo normal es void.
	//No debe llevar argumentos
	public void ejecutaDespuesCreacion() {
	
		System.out.println("Ejecutado tras ejecucion del Bean");
	}
	
	//Ejecucion de codigo despues del apagado del contenedor Spring
	
	@PreDestroy
	//Este metodo puede tener cualquier modificador de acceso y puede devolver cualquier valor, lo normal es void
	//No debe llevar argumentos
	public void ejecutaAntesDestruccion() {
		
		System.out.println("Ejecutamos antes de la destrucci�n");
	}
	
	//Los metodos init y destroy/postconstruct y predestroy no trabajan con el patron de dise�o prototype, solo singleton

}
