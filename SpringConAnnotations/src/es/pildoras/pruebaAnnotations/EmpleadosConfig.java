package es.pildoras.pruebaAnnotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("es.pildoras.pruebaAnnotations")
@PropertySource("classpath:datosEmpresa")
public class EmpleadosConfig {

	//definir el bean para InformeFinancieroDtoCompras
	
	@Bean
	public CreacionInformeFinanciero informeFinancieroDptoCompras() { //El nombre del metodo ser� el nombre id para pedir el bean
		
		return new InformeFinancieroDptoCompras(); //Retorna la clase de la cual quieres el objeto o bean
		
	}
	
	
	
	//definir el bean para DirectorFinanciero e inyectar dependencias
	@Bean
	public Empleados directorFinanciero() {
		
		return new DirectorFinanciero(informeFinancieroDptoCompras());//Aqui se produce una inyeccion donde el parametro sera el id del bean
		
	}
	
}
