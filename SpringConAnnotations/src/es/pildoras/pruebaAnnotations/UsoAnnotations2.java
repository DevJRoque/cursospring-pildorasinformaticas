package es.pildoras.pruebaAnnotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Cargar el xml de configuracion
			//ClassPathXmlApplicationContext contexto5=new ClassPathXmlApplicationContext("applicationContext2.xml"); comentado pq ya no usamos el archivo xml
		
		
		//Leer la clase de configuracion
		
		AnnotationConfigApplicationContext contexto=new AnnotationConfigApplicationContext(EmpleadosConfig.class);//sin comillas ""
		
		
		
		//Pedir bean
	/*	Empleados Antonio=contexto.getBean("ComercialExperimentado",Empleados.class); //el alias es el component de la clase
		
		Empleados Lucia=contexto.getBean("ComercialExperimentado",Empleados.class);*/
		
		
		//Empleados empleado=contexto.getBean("directorFinanciero",Empleados.class);
		
		DirectorFinanciero empleado=contexto.getBean("directorFinanciero",DirectorFinanciero.class);
		

		//System.out.println(empleado.getInforme());
		//System.out.println(empleado.getTareas());
		System.out.println(empleado.getEmail());
		System.out.println(empleado.getNombreEmpresa());
		
		
		contexto.close();
			} 
	
	
		
}
