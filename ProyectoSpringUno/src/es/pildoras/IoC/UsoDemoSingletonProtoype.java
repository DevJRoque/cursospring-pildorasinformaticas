package es.pildoras.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoDemoSingletonProtoype {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Carga xml configuracion
		
		ClassPathXmlApplicationContext contexto2=new ClassPathXmlApplicationContext("applicationContext2.xml");
		
		//Pedir beans al contenedor spring
		
		SecretarioEmpleado Maria=contexto2.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		SecretarioEmpleado Pedro=contexto2.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		
		if(Maria==Pedro) {
			System.out.println("Apuntan al mismo objeto");
		}else
			System.out.println("No apuntan al mismo objeto");
	}

}
