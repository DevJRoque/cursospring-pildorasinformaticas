package es.pildoras.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoCicloVidaBean {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Cargar xml configuracion
		
		ClassPathXmlApplicationContext contexto3=new ClassPathXmlApplicationContext("applicationContext3.xml");
		
		//Pedir bean al contenedor
		
		Empleados Juan=contexto3.getBean("miEmpleado",Empleados.class);
		
		System.out.println(Juan.getInforme());
		
		//Cerrar el contexto
		
		contexto3.close();
		
	}

}
