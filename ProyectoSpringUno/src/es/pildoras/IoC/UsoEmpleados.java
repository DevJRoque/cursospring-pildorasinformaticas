package es.pildoras.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Crear un contexto para usar el archivo de configuracion.Cargar XML
		
		ClassPathXmlApplicationContext contexto=new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Pide un alias y el nombre de la interfaz.Pedir el Bean
		
		DirectorEmpleado Juan=contexto.getBean("miEmpleado",DirectorEmpleado.class);
			
		System.out.println(Juan.getTareas());
		
		System.out.println(Juan.getInforme());
		System.out.println(Juan.getEmail());
		System.out.println(Juan.getNombreEmpresa());
		
		/*SecretarioEmpleado Maria=contexto.getBean("miSecretarioEmpleado",SecretarioEmpleado.class);
		
		System.out.println(Maria.getTareas());
		System.out.println(Maria.getInforme());
		System.out.println(Maria.getEmail());
		System.out.println(Maria.getNombreEmpresa());*/
		
		
		contexto.close();
		
		
		
	}

}
