package es.pildoras.IoC;

public class DirectorEmpleado implements Empleados {

	//Creacion de campo tipo CreacionInforme(interfaz)
	
	private CreacionInformes informeNuevo;
	private String email;
	private String nombreEmpresa;
	
	//Constructor donde se inyecta la dependencia
	public DirectorEmpleado(CreacionInformes informeNuevo) {
		
		this.informeNuevo=informeNuevo;
	}
	
	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestionar la plantilla de la empresa";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe creado por el director: "+ informeNuevo.getInforme();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	
	public void metodoInicial() {
		System.out.println("Metodo init. Tareas a ejecutar antes de la creacion del bean");
	}
	
	public void metodoFinal() {
		System.out.println("Metodo destroy. Tareas a ejecutar despues de utilizar el bean");
	}

}
